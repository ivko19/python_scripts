import requests
from bs4 import BeautifulSoup
from csv import writer
import datetime

# url = "https://www.halooglasi.com/nekretnine/prodaja-stanova?grad_id_l-lokacija_id_l-mikrolokacija_id_l=52180%2C52182%2C52195%2C52196%2C535589%2C535590%2C535591&cena_d_to=95000&cena_d_unit=4&kvadratura_d_from=33&kvadratura_d_to=65&kvadratura_d_unit=1&broj_soba_order_i_from=2&sprat_order_i_from=11&dodatno_id_ls=12000004"
# url = "https://www.halooglasi.com/nekretnine/prodaja-stanova?grad_id_l-lokacija_id_l-mikrolokacija_id_l=52180%2C52182%2C52195%2C52196%2C535589%2C535590%2C535591&cena_d_to=95000&cena_d_unit=4&kvadratura_d_from=33&kvadratura_d_to=65&kvadratura_d_unit=1&broj_soba_order_i_from=2&sprat_order_i_from=11&dodatno_id_ls=12000004&page=4"
# response = requests.get(url, headers={'User-Agent': 'Chrome/51.0.2704.103'})
# supa = BeautifulSoup(response.text, "html.parser")
# print(response.status_code)

promenjiva=True
i=1
brojac=1

#određivanje datuma
# datum = datetime.datetime.now()
# print(x.strftime("%y-%b-%d"))
# datum = datum.strftime("%x")
datum=datetime.date.today()
# print(datum/)
# kod koji ubacuje stranicu u csv



with open(f"halo_{datum}.csv", "w") as csv_file:
    csv_writer = writer(csv_file)
    str="id,naslov,lokacija,kvadratura,struktura,cena,url,datum"
    print(str)
    csv_writer.writerow([str])

    while promenjiva:
        url = f"https://www.halooglasi.com/nekretnine/prodaja-stanova?grad_id_l-lokacija_id_l-mikrolokacija_id_l=52180%2C52182%2C52195%2C52196%2C535589%2C535590%2C535591&cena_d_to=95000&cena_d_unit=4&kvadratura_d_from=33&kvadratura_d_to=65&kvadratura_d_unit=1&broj_soba_order_i_from=2&sprat_order_i_from=11&dodatno_id_ls=12000004&page={i}"
        response = requests.get(url, headers={'User-Agent': 'Chrome/51.0.2704.103'})
        supa = BeautifulSoup(response.text, "html.parser")
        oglasi = supa.find_all(class_="product-item product-list-item Standard real-estates my-product-placeholder")
        print(f"{i}ti prolaz")
        i+=1
        if len(oglasi)>0:
            promenjiva = True
        else:
            promenjiva = False
        print(promenjiva)

        klase = supa.find_all(class_="col-md-12 col-sm-12 col-xs-12 col-lg-12")
        for klasa in klase:
            h_tag = klasa.find("h3")
            naslov = h_tag.a.get_text()
            # naslov = a_tag.img["alt"]

            a_tag = klasa.find("a")
            tempurl = a_tag['href']
            id_oglasa = tempurl[-19:-6]

            cena = klasa.find(class_="central-feature")
            price = cena.get_text()   #regex kod <br>
            location = klasa.find(class_="subtitle-places")
            lokacija = location.get_text()  # regex kod <br>

            strukt = klasa.find_all(class_="value-wrapper")
            struktura = strukt[2].get_text()[:-10]
            kvadratura = strukt[1].get_text()[:-10]

            print(f"ID OGLASA: {id_oglasa}")
            print(f"NAZIV: {naslov}")
            print(f"CENA: {price}, ")
            print(f"LOKACIJA: {lokacija}, ")
            print(f"STRUKTURA: {struktura}, ")
            print(f"KVADRATURA: {kvadratura}, ")
            print(f"LINK: www.halooglasi.com{tempurl}")

            brojac+=1
            print()
            csv_writer.writerow([id_oglasa, naslov, lokacija, kvadratura, struktura, price, f"www.halooglasi.com{tempurl}", datum])

print(brojac)



# with open("moj.csv", "w") as csv_file:
#     csv_writer = writer(csv_file)
#     str="title,link,date"
#     print(str)
#     csv_writer.writerow([str])
#
#     for artikal in artikli:
#         artikal.find()
#         print(artikal)
#         a_tag = artikal.find("a")
#         title = a_tag.get_text()
#         url = a_tag['href']
#         date = artikal.find("time")['datetime']
#         csv_writer.writerow([title, url, date])