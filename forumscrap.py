import requests
from bs4 import BeautifulSoup
from csv import writer
import datetime

# for petlja da se prolazi kroz strane urla
total_pages=830
#postaviš range koji hoćeš
for j in range(820,total_pages):
    #
    url = (f"https://vox92.net/forum/index.php?/topic/1176-epidemija-koronavirusa-covid-19-sars-cov2-dnevne-aktuelnosti-iz-zemlje-i-sveta/page/{j}/")
    print(f"***************strana: {j} od {total_pages} ***********************************")
    print()

    # prijem responsa i parsiranje
    response = requests.get(url)
    supa = BeautifulSoup(response.text, "html.parser")


    artikli=supa.find_all("article")
    # izdvajanje podataka iz articlea. tražimo po klasi, i konkretnom autoru posta
    for artikal in artikli:
        print("------------------------------------------")
        #mislim da mi ovde treba ime i ime2, zbog dole
        ime2=artikal.find(class_="cAuthorPane_mobile ipsResponsive_showPhone")
        ime=ime2.find("img")['alt']
        # ime=ime2['alt']
        if ime=="zoe Bg":
            # izdvajamo sadržaj posta uklanjamo CRove, uzimamo vreme, kropujemo samo datum.
            pitanje=artikal.find(class_="cPost_contentWrap").find("div").get_text().replace('\n', ':')
            ime2 = ime2.find("time")
            time=ime2["datetime"]
            if pitanje.find("/") != -1:
                # ako post sadrzi karakter / - unosimo u csv
                with open(f"Zoe.csv", "a") as csv_file:
                    csv_writer = writer(csv_file)
                    csv_writer.writerow([time[:10], pitanje])

    print()

