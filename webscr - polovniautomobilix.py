import requests
from bs4 import BeautifulSoup
from csv import writer
import datetime


url = "https://www.polovniautomobili.com/auto-oglasi/pretraga?brand=&price_from=&price_to=8000&year_from=2010&year_to=&chassis%5B%5D=277&chassis%5B%5D=2631&fuel%5B%5D=45&fuel%5B%5D=2310&flywheel=&atest=&door_num=3013&submit_1=&without_price=1&seller_type%5B%5D=persons&date_limit=&showOldNew=all&modeltxt=&engine_volume_from=1350&engine_volume_to=&power_from=66&power_to=&mileage_from=&mileage_to=&emission_class=&gearbox%5B%5D=3210&gearbox%5B%5D=3211&gearbox%5B%5D=3212&seat_num=&wheel_side=&air_condition%5B%5D=3159&air_condition%5B%5D=3160&registration=&country=&country_origin=&origin%5B%5D=2687&ownership=&city=&registration_price=&page=2&sort=tagValue218_desc"
response = requests.get(url, headers={'User-Agent': 'Chrome/51.0.2704.103'})
supa = BeautifulSoup(response.text, "html.parser")

jsklase=supa.find_all(class_="js-hide-on-filter")
divklase=jsklase[1].contents[1]
# print(divklase.get_text())
brojevi=[int(s) for s in divklase.get_text().split() if s.isdigit()]
# print(brojevi)
total_pages=brojevi[2]//brojevi[1]+1
# print(total_pages)
aa_tag = jsklase[1].find("a")
parturl=aa_tag['href'][28:]
# print(aa_tag['href'][:])


#određivanje datuma
datum = datetime.datetime.now()
# print(x.strftime("%y-%b-%d"))
datum = datum.strftime("%x")

# kod koji ubacuje stranicu u csv

klase = supa.find_all(class_="single-classified")


with open("moj.csv", "w") as csv_file:
    csv_writer = writer(csv_file)
    str="id,naslov,kubikaza,datum"
    print(str)
    csv_writer.writerow([str])

    for klasa in klase:
        print("------------------------------------------")
        # print(f"oglas {i} od {len(klase)+1}. ")
        # i+=1
        a_tag = klasa.find("a")
        naslov = a_tag['title']
        # naslov = a_tag.get_text()
        print(f"NAZIV: {naslov}")

        tempurl = a_tag['href']
        print(f"LINK: www.polovniautomobili.com{tempurl}")

        cena = klasa.find(class_="price")
        cena = cena.get_text()
        print(f"CENA: {cena}")
        id_oglasa = tempurl[13:21]
        print(f"ID OGLASA: {id_oglasa}")

        osobine = klasa.find_all(class_="inline-block")

        print(f"GODISTE: {osobine[0].get_text()[:-4]}")
        print(f"KILOMETRAZA: {osobine[1].get_text()[:-2]}")
        print(f"KUBIKAZA: {osobine[3].get_text()[:-2]}")
        print(f"SNAGA: {osobine[5].get_text()[:-2]}")

        # print(f"GODISTE: {osobine[2].contents[1].get_text()[:-4]}")
        # print(f"KILOMETRAZA: {osobine[2].contents[3].get_text()[:-2]}")
        # print(f"KUBIKAZA: {osobine[2].contents[7].get_text()[:-2]}")
        # print(f"SNAGA: {osobine[2].contents[11].get_text()[:-2]}")

        csv_writer.writerow([id_oglasa,naslov,cena,osobine[3].get_text()[:-2],datum])
        # csv_writer.writerow([naslov,cena,osobine[2].contents[7].get_text()[:-2],datum])



for j in range(2,total_pages+1):
    # print (type(j))
    # jj = str (j)
    url2 = (f"https://www.polovniautomobili.com/auto-oglasi/pretraga?page={j}{parturl}")
    print(f"***************strana: {j} od {total_pages} ***********************************")
    print()
    print(url2)

    response = requests.get(url2, headers={'User-Agent': 'Chrome/51.0.2704.103'})
    supa = BeautifulSoup(response.text, "html.parser")

    klase = supa.find_all(class_="single-classified")

    with open("moj.csv", "a") as csv_file:
        csv_writer = writer(csv_file)

        for klasa in klase:
            print("------------------------------------------")
            a_tag = klasa.find("a")
            naslov = a_tag['title']
            print(f"NAZIV: {naslov}")

            tempurl = a_tag['href']
            print(f"LINK: www.polovniautomobili.com{tempurl}")

            cena = klasa.find(class_="price")
            cena = cena.get_text()
            print(f"CENA: {cena}")
            id_oglasa = tempurl[13:21]
            print(f"ID OGLASA: {id_oglasa}")

            osobine = klasa.find_all(class_="inline-block")


            print(f"GODISTE: {osobine[0].get_text()[:-4]}")
            print(f"KILOMETRAZA: {osobine[1].get_text()[:-2]}")
            print(f"KUBIKAZA: {osobine[3].get_text()[:-2]}")
            print(f"SNAGA: {osobine[5].get_text()[:-2]}")

            # print(f"GODISTE: {osobine[2].contents[1].get_text()[:-4]}")
            # print(f"KILOMETRAZA: {osobine[2].contents[3].get_text()[:-2]}")
            # print(f"KUBIKAZA: {osobine[2].contents[7].get_text()[:-2]}")
            # print(f"SNAGA: {osobine[2].contents[11].get_text()[:-2]}")

            csv_writer.writerow([id_oglasa, naslov, cena, osobine[3].get_text()[:-2], datum])

    print()

