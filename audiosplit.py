import datetime
import math
import wave
from pydub import AudioSegment
import csv
from pathlib import Path

def cropaudio(oldfilename,newfilename,number,time1,time2):
    print(f"stari:{oldfilename}, Novi:{newfilename}")
    time1 = time1 / 1000 #Works in milliseconds
    time2 = time2 / 1000
    newAudio = AudioSegment.from_wav(oldfilename)
    newAudio = newAudio[time1:time2]
    newAudio.export(f"{number} - {newfilename}.wav", format="wav") #Exports to a wav file in the current path.


# expecting 16 bit PCM 44100 Hz
fajl='Riblja Corba - Kost U Grlu.wav'

with open('DW_A0084.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        else:
            p=datetime.datetime.strptime(row[2],'%M:%S:%f')
            t=datetime.datetime.strptime(row[3], '%M:%S:%f')
            p=p.microsecond+p.second*1000000+p.minute*60*1000000
            t=t.microsecond+t.second*1000000+t.minute*60*1000000

            print(f'broj pesme: {row[0]}, naziv: {row[1]}, početak {p},{type(p)} kraj {t},{type(t)}')

            cropaudio(fajl,row[1],row[0],p,t)
            line_count += 1

    print(f'Processed {line_count} lines.')


